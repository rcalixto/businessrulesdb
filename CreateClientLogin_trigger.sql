-- trigger create client login on investor's area
create function create_client_login_fn()
returns trigger as $BODY$
begin
    insert into investors_area.ia_login (email, password, disclaimer, totp, client) values
    (new.email, '$2b$12$XO2nnckCJWZjcrt38VUjRONOvM0QpeQk8H5NDZuQ2y4.uA9scRhmu', true, 'false', new.id);
    return new;
end;
$BODY$ language 'plpgsql';

create trigger create_client_login_tg
after insert on business.client
for each row
execute procedure create_client_login_fn();
