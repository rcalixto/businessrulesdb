/* This function blocks new investments on inative lots */
create function check_investment_lot()
returns trigger as $BODY$
declare
    status bool;
begin
    select active into status from business.lot where lot = new.lot;
    if (not status) then
        delete from business.investment where id = new.id;
    end if;
    return new;
end;
$BODY$ language 'plpgsql';

create trigger just_active_lots
after insert on business.investment
for each row
execute procedure check_investment_lot();
