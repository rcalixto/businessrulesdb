/* This function activate an inative client in case a new investment is seted
 on his name */
create function active_client()
returns trigger as $BODY$
declare
    status bool;
begin
    select active into status from business.client where id = new.client;
    if (not status) then
        update business.client set active = True;
    end if;
    return new;
end;
$BODY$ language 'plpgsql';

create trigger active_new_client
after insert on business.investment
for each row
execute procedure active_client();
